<?php get_header(); ?>

<section id="content">
  <header class="header">
    <div class="row">
      <div class="col-md-12">
        <h1><?php _e( 'Página não encontrada!', 'espacoarte' ); ?></h1>
      </div>
    </div>
  </header>
  <section>
    <div class="row">
      <div class="col-md-9">
        <p><?php _e( 'A página solicitada não está disponível. Tente novamente ou faça uma busca.', 'espacoarte' ); ?></p>
        <?php get_search_form(); ?>
      </div>
      <dlv class="col-md-3">
        <?php get_sidebar(); ?>
      </dlv>
    </div>
  </section>
</section>
<?php get_footer(); ?>