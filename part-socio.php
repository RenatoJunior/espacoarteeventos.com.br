<div class="cadastros-home box">
  <h3>Seja Associado</h3>
  <div class="box-conteudo">
    <p>Vantagens ao associar-se:</p>
    <ul>
      <li>Revista de Direito Constitucional e Internacional
      <li>Gratuidade nos Congressos</li>
      <!--<li>Acesso ao Sistema ML&R Adv.</li>-->
    </ul>
    <a href="<?php echo esc_url(home_url('/')); ?>associe-se/" class="btn-padrao"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Associe-se</a>
  </div>
</div>