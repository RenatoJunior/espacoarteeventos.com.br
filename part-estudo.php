<div class="cadastros-home box">
  <h3>Grupos de estudo</h3>
  <div class="box-conteudo">
    <p>Participe de grupos de estudo com a Profª. Maria Garcia.</p>
    <a href="<?php echo esc_url(home_url('/')); ?>grupos-de-estudo/" class="btn-padrao"><span class="glyphicon glyphicon-pencil"></span> Quero Participar</a>
  </div>
</div>