jQuery(function ($) {

  //NÚMERO DE AUTORES
  $('input[name=autores]').click(function () {
    var suporte = $('input[name=autores]:checked').val();
    for ($i = 1; $i <= 6; $i++) {
      if ($i <= suporte) {
        $('#' + $i).css('display', 'block');
      }
      if ($i > suporte) {
        $('#' + $i).css('display', 'none');
      }
    }
  });

  $("#tabela input").keyup(function () {
    var index = $(this).parent().index();
    var nth = "#tabela td:nth-child(" + (index + 1).toString() + ")";
    var valor = $(this).val().toUpperCase();
    $("#tabela tbody tr").show();
    $(nth).each(function () {
      if ($(this).text().toUpperCase().indexOf(valor) < 0) {
        $(this).parent().hide();
      }
    });
  });

  $("#tabela input").blur(function () {
    $(this).val("");
  });
});