<?php ?>
<div class="container">
  <div class="row head-content">
    <div class="col-sm-3">
      <div class="logo">
        <h1>E<span>sp</span>açoArte</h1>
        <p>Eventos</p>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="face">
        <span class="fa fa-facebook-square"></span> <span>espacoarteeventos</span>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="contatos">
        <span class="tel">9 8932-3806</span>
        <span class="email">contatos@espacoarteeventos.com.br</span>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="endereco">
        <span class="fa fa-map-marker"></span>
        <span class="end"><strong>Rua Quatá, 684</strong><br>Vila Olimpia SP</span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-3">Foto 01</div>
    <div class="col-sm-9">
      <div class="row">
        <div class="col-sm-6">Foto 02</div>
        <div class="col-sm-6">Foto 03</div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="stripe">
        <p>Agilidade e conveniência são determinantes quando empresas buscam o espaço ideal para realizarem seus eventos. Localizado na principal via de acesso à Vila Olímpia, tem a infra-estrutura certa para que o seu evento aconteça e seus objetivos sejam alcançados.</p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-3">
      Foto 01
      <p>Corporativo</p>
    </div>
    <div class="col-sm-3">
      Foto 02
      <p>Social</p>
    </div>
    <div class="col-sm-3">
      Foto 03
      <p>Artístico</p>
    </div>
    <div class="col-sm-3">
      Foto 04
      <p>Gastronômico</p>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <h2 class="title">Infraestrutura</h2>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-9">
      <p>OS 450m2 do ESPAÇOARTE propiciam que todos os eventos aconteçam em ambiente intimista, aproximando as pessoas.</p>
      <p>As estruturas oferecem iluminação, climatização e iluminação do ambiente e palco, com área VIP conferem conveniência e convivência.</p>
      <h4>INFRAESTRUTURA</h4>
      <ul>
        <li>Som e Imagem</li>
        <li>Ar-Condicionado</li>
        <li>Área VIP</li>
      </ul>
      <h4>SERVIÇOS</h4>
      <ul>
        <li>Coordenadoria</li>
        <li>Auxiliar de Limpeza</li>
        <li>Responsável Técnico de Manutenção</li>
        <li>Limpeza Antes e Pós-Evento</li>
        <li>Valet (pago à parte)</li>
      </ul>
      <h4>ÁREAS</h4>
      <ul>
        <li>Total: 450m2</li>
        <li>Térreo: 200m2</li>
        <li>Mezanino: 150m2</li>
        <li>Área externa: 100m2</li>
        <li>Pé direito: 6m</li>
      </ul>
      <h4>Formatos e Capacidades</h4>
      <ul>
        <li>Coquetel: 220 pessoas</li>
        <li>Auditório: 114 pessoas</li>
        <li>Jantar: Entre 60 e 80 pessoas (opções com ou sem palco)</li>
      </ul>
    </div>
    <div class="col-sm-3">
      <p>Opções de Plantas</p>
      <p>Opção 01</p>
      <p>Opção 02</p>
      <p>Opção 03</p>
      <p>Opção 04</p>
      <p>Opção 05</p>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <h2 class="title">Equipamentos</h2>
      <table class="table">
        <tr>
          <td class="td-title"><p>Cozinha / Bar</p></td>
          <td>Imagens</td>
        </tr>
        <tr>
          <td class="td-title"><p>Mobiliário</p></td>
          <td>Imagens</td>
        </tr>
        <tr>
          <td class="td-title"><p>Iluminação / Projeção</p></td>
          <td>Imagens</td>
        </tr>
        <tr>
          <td class="td-title"><p>PA Audio</p></td>
          <td>Imagens</td>
        </tr>
      </table>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <h2 class="title">Blog</h2>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-3">
      Foto 01
      <p>Corporativo</p>
    </div>
    <div class="col-sm-3">
      Foto 02
      <p>Social</p>
    </div>
    <div class="col-sm-3">
      Foto 03
      <p>Artístico</p>
    </div>
    <div class="col-sm-3">
      Foto 04
      <p>Gastronômico</p>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <h2 class="title">Contato/Localização</h2>
    </div>
  </div>

</div>

<div class="clearfix"></div>
<?php  ?>