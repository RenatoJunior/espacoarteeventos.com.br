<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width" />
    <title><?php wp_title(' | ', true, 'right'); ?></title>
    <link rel="icon" type="image/x-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/font-awesome.min.css" />
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <header id="header" role="topo">
      
      <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-example-js-navbar-collapse">
            <span>Menu <span class="fa fa-bars"></span></span>
          </button>
        </div>
        <div class="collapse navbar-collapse bs-example-js-navbar-collapse">
          <?php
          wp_nav_menu(array(
              'theme_location' => 'primary',
              'container' => 'nav-collapse collapse navbar-inverse-collapse',
              'menu_class' => 'nav navbar-nav navbar-right',
              'fallback_cb' => 'webriti_fallback_page_menu',
              'walker' => new busiprof_nav_walker())
          );
          ?>  
        </div>
      </div>
    </nav>

    <div class="container">
      <div class="row head-content">
        <div class="col-sm-3">
          <div class="logo">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-espacoarte.jpg" alt="Logo Espaço Arte Eventos" class="logo-home">
          </div>
        </div>
        <div class="col-sm-3">
          <div class="face">
            <span class="fa fa-facebook-square"></span> <span>espacoarteeventos</span>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="contatos">
            <span class="tel">9 8932-3806</span>
            <span class="email">contatos@espacoarteeventos.com.br</span>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="endereco">
            <span class="fa fa-map-marker"></span>
            <span class="end"><strong>Rua Quatá, 684</strong><br>Vila Olimpia SP</span>
          </div>
        </div>
      </div>
    </div>
      
    </header>
    <div class="container">