<div id="sidebar" role="complementary" class="esconder">
  <?php if (is_active_sidebar('primary-widget-area')) : ?>
    <?php dynamic_sidebar('primary-widget-area'); ?>
  <?php endif; ?>
  <?php get_template_part("part-socio") ?>
  <?php get_template_part("part-artigo") ?>
  <?php get_template_part("part-estudo") ?>
</div>