<div class="searchform">
  <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" accept-charset="utf-8" id="searchform" role="search">
      <input type="text" class="txt-padrao" name="s" id="s" value="<?php the_search_query(); ?>" />
      <button type="submit" id="searchsubmit"class="btn-padrao"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
  </form>
</div>
