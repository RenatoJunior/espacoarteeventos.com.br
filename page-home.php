<?php
/* Template Name: Home */
get_header(); ?>

  <div class="row">
    <div class="col-sm-12">
      <h2 class="title">Infraestrutura</h2>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-9">
      <div class="conteudo-infra">
        <p>OS 450m2 do ESPAÇOARTE propiciam que todos os eventos aconteçam em ambiente intimista, aproximando as pessoas.</p>
        <p>As estruturas oferecem iluminação, climatização e iluminação do ambiente e palco, com área VIP conferem conveniência e convivência.</p>
        <h4>INFRAESTRUTURA</h4>
        <ul>
          <li>Som e Imagem</li>
          <li>Ar-Condicionado</li>
          <li>Área VIP</li>
        </ul>
        <h4>SERVIÇOS</h4>
        <ul>
          <li>Coordenadoria</li>
          <li>Auxiliar de Limpeza</li>
          <li>Responsável Técnico de Manutenção</li>
          <li>Limpeza Antes e Pós-Evento</li>
          <li>Valet (pago à parte)</li>
        </ul>
        <h4>ÁREAS</h4>
        <ul>
          <li>Total: 450m2</li>
          <li>Térreo: 200m2</li>
          <li>Mezanino: 150m2</li>
          <li>Área externa: 100m2</li>
          <li>Pé direito: 6m</li>
        </ul>
        <h4>Formatos e Capacidades</h4>
        <ul>
          <li>Coquetel: 220 pessoas</li>
          <li>Auditório: 114 pessoas</li>
          <li>Jantar: Entre 60 e 80 pessoas (opções com ou sem palco)</li>
        </ul>
      </div>
    </div>
    <div class="col-sm-3">
      <p>Opções de Plantas</p>
      <p>Opção 01</p>
      <p>Opção 02</p>
      <p>Opção 03</p>
      <p>Opção 04</p>
      <p>Opção 05</p>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <h2 class="title">Equipamentos</h2>
      <table class="table">
        <tr>
          <td class="td-title"><p>Cozinha / Bar</p></td>
          <td>Imagens</td>
        </tr>
        <tr>
          <td class="td-title"><p>Mobiliário</p></td>
          <td>Imagens</td>
        </tr>
        <tr>
          <td class="td-title"><p>Iluminação / Projeção</p></td>
          <td>Imagens</td>
        </tr>
        <tr>
          <td class="td-title"><p>PA Audio</p></td>
          <td>Imagens</td>
        </tr>
      </table>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <h2 class="title">Blog</h2>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-3">
      Foto 01
      <p>Corporativo</p>
    </div>
    <div class="col-sm-3">
      Foto 02
      <p>Social</p>
    </div>
    <div class="col-sm-3">
      Foto 03
      <p>Artístico</p>
    </div>
    <div class="col-sm-3">
      Foto 04
      <p>Gastronômico</p>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <h2 class="title">Contato/Localização</h2>
    </div>
  </div>

</div>

<div class="clearfix"></div>

<section id="content">
  <section id="banner">
    <div class="row">
      <div class="col-sm-12">
        <?php if (shortcode_exists( 'simple-slider' )) : do_shortcode( '[simple-slider]' ); endif; ?>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
          <?php the_content(); ?>
        <?php endwhile;endif; ?>
      </div>
    </div>
  </section>
  
  <section id="grupos_de_estudo">
    <h2><span class="glyphicon glyphicon-education" aria-hidden="true"></span>Grupos de Estudo</h2>
    <div class="row">
      <?php query_posts("showposts=3&cat=3"); ?>
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <div class="col-sm-4">
        <a class="grupos-home box" href="<?php the_permalink() ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
          <?php if (has_post_thumbnail( $post->ID ) ): ?>
          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
          <div class="box-imagem eventos-imagem">
            <img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
          </div>
          <h3 class="box-titulo"><?php the_title(); ?></h3>
          <?php endif; ?>
        </a>
      </div>
      <?php endwhile; endif; ?>
    </div>
  </section>
  <section id="eventos">
    <h2><span class="glyphicon glyphicon-camera" aria-hidden="true"></span>Eventos</h2>
    <div class="row">
      <div class="col-sm-8">
        <div class="row">
          <?php query_posts("showposts=1&cat=2"); ?>
          <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <div class="col-xs-12">
            <a class="eventos-home box" href="<?php the_permalink() ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
              <?php if (has_post_thumbnail( $post->ID ) ): ?>
              <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
              <div class="box-imagem">
                <img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
              </div>
              <?php endif; ?>
              <h3 class="box-titulo"><?php the_title(); ?></h3>
            </a>
          </div>
          <?php endwhile; endif; ?>
        </div>
      </div>
      <div class="col-sm-4">
        <?php get_template_part("part-artigo") ?>
        <?php get_template_part("part-estudo") ?>
      </div>
    </div>
  </section>
</section>
<?php get_footer();