<?php if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) return; ?>

<section id="comentarios">
  <?php
  if (have_comments()) :
    global $comments_by_type;
    $comments_by_type = &separate_comments($comments);
    if (!empty($comments_by_type['comment'])) :
  ?>
  <section id="lista-comentarios">
    <h3 class="titulo-commentarios"><?php comments_number(); ?></h3>
    <?php if (get_comment_pages_count() > 1) : ?>
      <nav id="comments-nav-above" class="comments-navigation" role="navigation">
        <div class="paginated-comments-links"><?php paginate_comments_links(); ?></div>
      </nav>
    <?php endif; ?>
    <ul>
      <?php wp_list_comments('type=comment'); ?>
    </ul>
    <?php if (get_comment_pages_count() > 1) : ?>
      <nav id="comments-nav-below" class="comments-navigation" role="navigation">
        <div class="paginated-comments-links"><?php paginate_comments_links(); ?></div>
      </nav>
    <?php endif; ?>
  </section>
  <?php endif;
    if (!empty($comments_by_type['pings'])) :
      $ping_count = count($comments_by_type['pings']);
      ?>
      <section id="trackbacks-list" class="comments">
        <h3 class="comments-title"><?php echo '<span class="ping-count">' . $ping_count . '</span> ' . ( $ping_count > 1 ? __('Trackbacks', 'negocio-bwf') : __('Trackback', 'negocio-bwf') ); ?></h3>
        <ul>
          <?php wp_list_comments('type=pings&callback=negocio-bwf_custom_pings'); ?>
        </ul>
      </section>
      <?php
    endif;
  endif;
  ?>
  <?php if (comments_open()) ?>  
  <div id="respond">
  <h3><?php comment_form_title('Deixe seu comentário', 'Deixe seu comentário para %s'); ?></h3>
  <div class="cancel-comment-reply">
    <small><?php cancel_comment_reply_link(); ?></small>
  </div>
  <?php if (get_option('comment_registration') && !is_user_logged_in()) : ?>
    <p>You must be <a href="<?php echo wp_login_url(get_permalink()); ?>">logged in</a> to post a comment.</p>
  <?php else : ?>
    <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
      <?php if (is_user_logged_in()) : ?>
        <label>Conectado como <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Desconectar desta conta">Desconectar?</a></label>
      <?php else:  ?>
        <label for="author">Nome: <small><?php if ($req) echo "*"; ?></small></label>
        <input type="text" class="txt-padrao txt-cinza" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> />
        <label for="email">Email: <small><?php if ($req) echo "*"; ?></small></label>
        <input type="text" class="txt-padrao txt-cinza"z name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> />
        <label for="url">Website: </label>
        <input type="text" class="txt-padrao txt-cinza" name="url" id="url" value="<?php echo esc_attr($comment_author_url); ?>" size="22" tabindex="3" />
        <label for="comment">Comentário:</label>
      <?php endif; ?>
      <!--<p><small><strong>XHTML:</strong> You can use these tags: <code><?php echo allowed_tags(); ?></code></small></p>-->
      <textarea name="comment" class="txt-padrao txt-cinza" id="comment" cols="100%" rows="10" tabindex="4"></textarea>
      <button name="submit" class="btn-padrao" type="submit" id="submit" tabindex="5" ><span class="glyphicon glyphicon-send"></span> Enviar</button>
        <?php comment_id_fields(); ?>
      <?php do_action('comment_form', $post->ID); ?>
    </form>
  <?php endif; // If registration required and not logged in ?>
</section>