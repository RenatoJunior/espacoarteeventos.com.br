<div class="cadastros-home box">
  <h3>Envie seu artigo</h3>
  <div class="box-conteudo">
    <p>O Espaço Arte abre um espaço para você enviar seus artigos com a possibilidade de serem publicados em nossa revista.</p>
    <a href="<?php echo esc_url(home_url('/')); ?>envie-seu-artigo/"  class="btn-padrao"><span class="glyphicon glyphicon-send"></span> Enviar</a>
  </div>
</div>