<?php get_header(); ?>
<section id="content">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="header">
      <div class="row">
        <div class="col-sm-12">
          <h1><?php the_title(); ?><small> <?php edit_post_link(); ?></small></h1>
        </div>
      </div>
    </header>
    <section>
      <div class="row">
        <div class="col-md-9 single">
          <?php if (has_post_thumbnail()) { the_post_thumbnail(); } ?>
          <?php the_content(); ?>
          <div class="links">
            <?php wp_link_pages(); ?>
          </div>
          <footer>
            <span class="tag"><?php the_tags(); ?></span>
            <?php if (!post_password_required()) { comments_template('', true); } ?>
          </footer> 
        </div>
        <dlv class="col-md-3">
          <?php get_sidebar(); ?>
        </dlv>
      </div>
    </section>
  </article>
  <?php endwhile;endif; ?>
</section>
<?php get_footer();