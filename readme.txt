IMPORTANT - PLEASE READ: http://hoststyle.com.br/concept/

YOU MAY DELETE THIS FILE AND ANY OTHER FILE(S) BEFORE STARTING YOUR PROJECT

If you're creating your own theme/client project open up all files and do a "Find and Replace All in All Files" on the word "espacoarte" with your own slug/prefix.

- - THEME - -

Espaço Arte WordPress Theme
Demo: http://wp-themes.com/espacoarte/
Download: http://wordpress.org/themes/espacoarte

- - DESCRIPTION - -

This theme is aimed at web pros, but is of course available and supported for anyone.

The bare essentials of a WordPress theme, no visual CSS styles added except for the CSS reset and the mandatory WP classes. Perfect for those who would like to build their own theme completely from scratch.

One custom menu and one widgetized sidebar to get you started.

If you'd like a jumpstart with a CSS framework and more custom menus, page templates and widgetized areas, checkout SuperSimple:

http://hoststyle.com.br/supersimple/

- - COPYRIGHT & LICENSE - -

In its unchanged/original state, Espaço Arte is...

© 2011-2014 HostStyle
GNU General Public License | https://www.gnu.org/licenses/gpl.html

...however, once you've significantly changed the theme to build your own unique
project, either for yourself or for a client under a different theme name (as is encouraged) you're entirely welcome to copyright and license that project as you see fit.

- - SUPPORT - -

http://hoststyle.com.br/forum/

Enjoy. Thanks, HostStyle | http://hoststyle.com.br/