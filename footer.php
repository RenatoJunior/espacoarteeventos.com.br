</div>
<footer id="footer" role="contentinfo">
  <div class="container">
    <div class="row">
      <div class="col-sm-3 col-sm-offset-0">
        <h3><span class="glyphicon glyphicon-menu-right"></span> Links</h3>
        <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
      </div>
      <div class="col-sm-4">
        <h3><span class="glyphicon glyphicon-menu-right"></span> Entre em contato</h3>
        <?php echo do_shortcode('[contact-form-7 id="83" title="Contato"]'); ?>
      </div>
      <div class="col-sm-5">
        <div class="info">
          <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php esc_attr_e(get_bloginfo('name'), 'espacoarte'); ?> - <?php bloginfo('description'); ?>" alt="<?php esc_attr_e(get_bloginfo('name'), 'espacoarte'); ?> - <?php bloginfo('description'); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri() ; ?>/img/logo-espacoarte.png" alt=""/></a>
          <label>
            Tel (11) 3159-4363 Fax (11) 3151-4911<br>
            Rua Teodoro de Beurepaire, 234 - Ipiranga<br>
            04279-030 - São Paulo/SP
          </label>
        </div>
      </div>
    </div>
  </div>
  <div id="copyright" role="copy">
    <?php
      echo sprintf(__('Todos os direitos Reservados - %1$s %2$s', 'espacoarte'), esc_html(get_bloginfo('name')), date('Y'));
      echo sprintf(__(' | Desenvolvimento %1$s.', 'espacoarte'), '<a href="http://hoststyle.com.br/">HostStyle</a>');
    ?>
  </div>
</footer>
<?php wp_footer(); ?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/scripts.js" type="text/javascript"></script>
</body>
</html>