<?php get_header(); ?>

<section id="content">
  <section>
    <?php if ( have_posts() ) : ?>
    <div class="row">
      <div class="col-sm-12">
        <h1 class="entry-title"><?php printf( __( 'Resultados da pequisa por: %s', 'espacoarte' ), get_search_query() ); ?></h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9">
      <div class="row">
          <?php $cont = 0; ?>
          <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <div class="col-xs-4">
            <a class="eventos-home box" href="<?php the_permalink() ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
              <?php if (has_post_thumbnail( $post->ID ) ): ?>
              <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
              <div class="box-imagem eventos-imagem">
                <img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
              </div>
              <?php else: ?>
              <div class="box-imagem eventos-imagem">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/default.jpg" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
              </div>
              <?php endif; ?>
              <h3 class="box-titulo"><?php the_title(); ?></h3>
            </a>
          </div>
          <?php $cont = $cont+1; ?>
          <?php if ($cont==3) { echo '</div><div class="row">'; $cont=0; } ?>
          <?php endwhile; endif; ?>
          <?php global $wp_query; if ( $wp_query->max_num_pages > 1 ) { ?>
          <nav id="nav-links" role="navigation">
            <div class="nav-previous"><?php next_posts_link(sprintf( __( '%s older', 'espacoarte' ), '<span class="meta-nav">&larr;</span>' ) ) ?></div>
            <div class="nav-next"><?php previous_posts_link(sprintf( __( 'newer %s', 'espacoarte' ), '<span class="meta-nav">&rarr;</span>' ) ) ?></div>
          </nav>
          <?php } ?>
          <div class="links">
            <?php wp_link_pages(); ?>
          </div>
        </div>
      </div>
    <?php else : ?>
    <div class="row">
      <div class="col-sm-12">
        <h1 class="entry-title"><?php _e( 'Nada encontrado!', 'espacoarte' ); ?></h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9">
        <p><?php _e( 'Desculpe-nos, nada foi encontrado em sua busca. Por favor, tente novamente.', 'espacoarte' ); ?></p>
        <?php get_search_form(); ?>
      </div>
    <?php endif; ?>
      <dlv class="col-md-3">
        <?php get_sidebar(); ?>
      </dlv>
    </div>
  </section>
</section>

<?php get_footer(); ?>