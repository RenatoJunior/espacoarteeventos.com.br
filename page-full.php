<?php
/* Template Name: Modelo sem Sidebar */
get_header(); ?>
<section id="content">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="header">
      <div class="row">
        <div class="col-md-12">
          <h1><?php if (has_post_thumbnail()) { the_post_thumbnail(); } ?><?php the_title(); ?><small> <?php edit_post_link(); ?></small></h1>
        </div>
      </div>
    </header>
    <section>
      <div class="row">
        <div class="col-md-12">
          <?php the_content(); ?>
          <div class="links">
            <?php wp_link_pages(); ?>
          </div>
        </div>
      </div>
    </section>
  </article>
  <?php endwhile;endif; ?>
</section>
<?php get_footer(); ?>